﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebapiLocalAmigos.Models;

namespace WebapiLocalAmigos.Util
{
    public class AmigosMock
    {
        public int Id { get; set; }

        private readonly AmigosModels[] Amigos = new AmigosModels[5];

        /*
         * Função que retorna uma lista de todos amigos cadastrados ,para
         * preencher o select no front.
         * Função de retorno no get da API
         */
        public List<AmigosModels> MockDados()
        {

            List<AmigosModels> lista = new List<AmigosModels>();

            Amigos[0] = new AmigosModels { Id = 0, Nome = "Dirceu", Lat = -23.6431744, Lng = -46.559759 , type = "parking"};
            Amigos[1] = new AmigosModels { Id = 1, Nome = "Franco", Lat = -23.7141877, Lng = -46.7041568, type = "parking"};
            Amigos[2] = new AmigosModels { Id = 2, Nome = "Kelly", Lat = -23.7151599, Lng = -46.6964218, type = "parking"};
            Amigos[3] = new AmigosModels { Id = 3, Nome = "Paulo", Lat = -23.5953165, Lng = -46.6823547, type = "parking"};
            Amigos[4] = new AmigosModels { Id = 4, Nome = "Akemi", Lat = -23.6233905, Lng = -46.6988589, type = "parking"};

            foreach (AmigosModels Amigo in Amigos)
            {
                lista.Add(Amigo);
            }
                
            return lista;
        }

        /*
         * Uma vez que o programador seleciona o ponto de partida, o front envia um get com id do amigo que ele se encontra e
         * está função cacula as distâncias que são acrescentadas no Dictionary e ordenado de forma crescente. É devolvido uma
         * lista com os 3 primeiros e colocado no maps do front.
         */
        public List<AmigosModels> AmigosProximos()
        {
            Dictionary<int, double> distancias = new Dictionary<int, double>();

            List<AmigosModels> lista = new List<AmigosModels>();

            int count = 0;

            Amigos[0] = new AmigosModels { Id = 0, Nome = "Dirceu", Lat = -23.6431744, Lng = -46.559759, type = "parking" };
            Amigos[1] = new AmigosModels { Id = 1, Nome = "Franco", Lat = -23.7141877, Lng = -46.7041568, type = "parking" };
            Amigos[2] = new AmigosModels { Id = 2, Nome = "Kelly", Lat = -23.7151599, Lng = -46.6964218, type = "parking" };
            Amigos[3] = new AmigosModels { Id = 3, Nome = "Paulo", Lat = -23.5953165, Lng = -46.6823547, type = "parking" };
            Amigos[4] = new AmigosModels { Id = 4, Nome = "Akemi", Lat = -23.6233905, Lng = -46.6988589, type = "parking" };



            foreach (AmigosModels Amigo in Amigos)
            {
                if (Id != Amigo.Id)
                {
                    double result = Math.Sqrt((Math.Pow(Math.Abs(Amigos[Id].Lat) - Math.Abs(Amigo.Lat), 2) + Math.Pow(Math.Abs(Amigos[Id].Lng) - Math.Abs(Amigo.Lng), 2)));
                    distancias.Add(Amigo.Id, result);
                }
            }

            foreach (var i in distancias.OrderBy(j => j.Value))
            {
                if (count < 3)
                {
                    lista.Add(Amigos[i.Key]);
                    count++;
                }
                // Console.WriteLine("{0}, {1}", i.Key, i.Value);
                // Console.WriteLine(i);
            }

            return lista;
        }
    }
}
