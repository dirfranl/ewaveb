﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;

namespace WebapiLocalAmigos.Util
{
    public class DAL
    {
        private static readonly string Server = "localhost";
        private static readonly string Database = "localamigos";
        private static readonly string User = "root";
        private static readonly string Password = "";
        private MySqlConnection Connection;

        private readonly string ConnectionString = $"Server={Server};Database={Database};Uid={User};Pwd={Password};Sslmode=none;";

        public DAL()
        {
            Connection = new MySqlConnection(ConnectionString);
            Connection.Open();
        }

        // Executa: INSERT, UPDATE, DELETE 
        public void ExecutarCommandSQL(string sql)
        {
            MySqlCommand Command = new MySqlCommand(sql, Connection);
            Command.ExecuteNonQuery();
        }

        //Retorna dados do banco
        public DataTable RetornaDataTable(String sql)
        {
            MySqlCommand Command = new MySqlCommand(sql, Connection);
            MySqlDataAdapter DataAdapter = new MySqlDataAdapter(Command);
            DataTable Dados = new DataTable();
            DataAdapter.Fill(Dados);
            return Dados;
        }
    }
}
