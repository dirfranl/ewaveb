﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using WebapiLocalAmigos.Models;
using WebapiLocalAmigos.Util;

namespace WebapiLocalAmigos.Controllers
{
    [Route("api/[controller]")]
    public class AmigosController : Controller
    {
        /************************************ USANDO JWT AUTENTICATION *********************************************/
        private IConfiguration _config;
        /**********************************************************************************************************/

        /************************************ SIMPLES TOKEN AUTENTICATION *********************************************/
        //Autenticacao AutenticacaoServico;

        //public AmigosController(IHttpContextAccessor context)
        //{
        //    AutenticacaoServico = new Autenticacao(context);
        //}
        /*************************************************************************************************************/


        /************************************ USANDO JWT AUTENTICATION *********************************************/

        public AmigosController(IConfiguration config)
        {
            _config = config;
        }


        //GET api/amigos
        [HttpGet, Authorize]
        public List<AmigosModels> Get()
        {
            return new AmigosMock().MockDados();
        }

        /**********************************************************************************************************/

        /************************************ SIMPLES TOKEN AUTENTICATION *********************************************/
        //// GET api/amigos
        //[HttpGet]
        //public List<AmigosModels> Get()
        //{
        //    try {
        //        AutenticacaoServico.Autenticar();
        //        return new AmigosMock().MockDados();
        //    }
        //    catch
        //    {
        //        throw;
        //    }
        //}
        /*************************************************************************************************************/

        // GET api/amigos/5
        [HttpGet("{id}")]
        public List<AmigosModels> Get(int id)
        {
            return new AmigosMock{ Id = id }.AmigosProximos();
        }

        /************************************ USANDO JWT AUTENTICATION *********************************************/
        [AllowAnonymous]
        [HttpPost]
        public IActionResult CreateToken([FromBody]LoginModel login)
        {
            IActionResult response = Unauthorized();
            var user = Authenticate(login);

            if (user != null)
            {
                var tokenString = BuildToken(user);
                response = Ok(new { token = tokenString });
            }

            return response;
        }

        private string BuildToken(UserModel user)
        {
            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_config["Jwt:Key"]));
            var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);

            var token = new JwtSecurityToken(_config["Jwt:Issuer"],
              _config["Jwt:Issuer"],
              expires: DateTime.Now.AddMinutes(30),
              signingCredentials: creds);

            return new JwtSecurityTokenHandler().WriteToken(token);
        }

        private UserModel Authenticate(LoginModel login)
        {
            UserModel user = null;

            if (login.Username == "user@user.com" && login.Password == "senha")
            {
                user = new UserModel { Name = "Usuario Logado", Email = "user@user.com" };
            }
            return user;
        }

        public class LoginModel
        {
            public string Username { get; set; }
            public string Password { get; set; }
        }

        private class UserModel
        {
            public string Name { get; set; }
            public string Email { get; set; }
            public DateTime Birthdate { get; set; }
        }

        /**********************************************************************************************************/

        /*
         * Abaixo foi criado para usar futuramente para novos registros de amigos enviados pelo front.
         * Cadastro testado com sucesso.
         */
        // POST api/values
        //[HttpPost]
        //[Route("registraramigo")]
        //public ReturnAllServices RegistrarAmigo([FromBody]AmigosModels dados)
        //{
        //    ReturnAllServices retorno = new ReturnAllServices();
        //    try
        //    {
        //        dados.RegistrarAmigo();
        //        retorno.Result = true;
        //        retorno.ErrorMessage = string.Empty;
        //    }
        //    catch(Exception ex)
        //    {
        //        retorno.Result = false;
        //        retorno.ErrorMessage = "Erro ao tentar cadastrar um Amigo " + ex.Message;
        //    }
        //    return retorno;
        //}

        // PUT api/values/5
        //[HttpPut("{id}")]
        //public void Put(int id, [FromBody]string value)
        //{
        //}

        // DELETE api/values/5
        //[HttpDelete("{id}")]
        //public void Delete(int id)
        //{
        //}
    }
}
