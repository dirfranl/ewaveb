﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebapiLocalAmigos.Util;

namespace WebapiLocalAmigos.Models
{
    public class AmigosModels
    {
        public int Id { get; set; }

        public string Nome { get; set; }

        public double Lat { get; set; }

        public double Lng { get; set; }

        public string type { get; set; }


        public void RegistrarAmigo()
        {
            DAL objDAL = new DAL();

            string sql = "insert into amigos(nome,lat,lng)" + $"values('{Nome}', '{Lat}', '{Lng}')";

            objDAL.ExecutarCommandSQL(sql);
        }
    }
}
